const sendJSON = (res, json) => {
    res.write(JSON.stringify(json))
    res.end()
}

const setResponseJSON = () => ({ 'Content-Type': 'application/json' })

const setResponseSucess = (res, header) => {
    res.writeHead(200, header)
    return res
}

const getResponseFindOneSucess = (data) => ({
    status: "sucess",
    message: `Consulta retornada com sucesso`,
    data
})

module.exports = {
    sendJSON,
    setResponseJSON,
    setResponseSucess,
    getResponseFindOneSucess,
}
