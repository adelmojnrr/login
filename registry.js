const byPath = (url) => ({ path }) => path === url

const getRoutes = (router, method = 'get') =>
    router
        .routes
            .filter(route => route.method.toLowerCase() === method.toLowerCase())

module.exports = {
    use: (router) =>  (req, res) => {
        const url = req.url
        const method = req.method.toLowerCase()

    switch(method){
        case 'get': {
            getRoutes(router, 'get')
                .find(byPath(url))
                .action(req, res)
            break
        }

        default:
        break
}

}
}
