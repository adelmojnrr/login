const {
    sendJSON,
    setResponseJSON,
    setResponseSucess,
    getResponseFindOneSucess,
} = require('./helpers.js')

const actions = ({
    send: (req, res) => (data) => {
        return sendJSON(
            setResponseSucess(res, setResponseJSON),
            getResponseFindOneSucess(data)
        )
    }
})

module.exports = actions

